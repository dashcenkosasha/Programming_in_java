import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);

        int n = x.nextInt();
        int m = x.nextInt();
        int k = x.nextInt();
        if( 1<=k && k<=m && m<=n && n<=(Math.pow(10, 4)) ) {
            int q = n * k;
            int s =0 ;
            for (int i = 1; q>0 ; i++ ){
                q = q - m;
                if(q <= 0){
                    s = i;
                }
            }
            System.out.print(s);
        }
    }
}
