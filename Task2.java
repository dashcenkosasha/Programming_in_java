import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        int p = 0, N = x.nextInt();
        if (N >= 1 && N <= 2000000000) {
            for (long i = 1; N > i; i *= 2) {
                p++;
            }
            System.out.printf("%s", p);
        }
    }
}