import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        boolean bool = true;
        int voz = 0, ub = 0;

        //приняли 4 числа и записали в массив h[4].
        int[] h = new int[4];

        for (int j = 0; j < 4; j++) {
            h[j] = x.nextInt();
            if ((h[j]) < 0 || (h[j]) > 300) {
                bool = false;
            }
        }

        if(bool) {
            for (int j = 1; j < 4; j++) {
                if ((h[j]) > (h[j - 1])) {
                    voz++;
                } else {
                    ub++;
                }
            }

            if(voz == 0 || ub == 0){
                System.out.print("YES");
            } else {
                System.out.print("NO");
            }
        }
    }
}
