import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner x = new Scanner (System.in);
        long L = x.nextLong(), R = x.nextLong(), cash = 0;
        if (L>=1 && L <= (Math.pow(10, 18)) && R >=1 && R <= (Math.pow(10, 18))) {
            for (long j = L; j <= R; j++) {
                long m = 10, q = 0, chek = 0;
                long[] number = new long[20];
                for (int i = 0; q < j; i++) {
                    q = j % m;
                    number[i] = (j % m - (j % (m / 10))) / (m / 10);
                    m = m * 10;
                }
                q = 1;
                for (int y = 1; y < 20; y++) {
                    chek++;
                    if (number[y] == number[y - 1]) {
                        q++;
                    }
                }

                if (q == chek) {
                    cash++;
                }
            }
            System.out.println(cash);
        }
    }
}