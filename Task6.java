import java.util.Scanner;

public class Task6 {
    public static void main(String[] args){
        Scanner x= new Scanner(System.in);
        boolean bool = true;

        int n = x.nextInt(), f = 0, d = 0;
        if (n<2 || n>1000){
            bool = false;
        }


            int[] height = new int[n];
        if (bool) {
            for (int j = 0; j < n; j++) {
                height[j] = x.nextInt();
                if ((height[j]) < 1 || (height[j]) > (Math.pow(10, 9))) {
                    bool = false;
                }
            }
        }
            if (bool) {
                //Определяем четность числа и записываем в ячейку [n][1], где: 1 - нечетное; 0 - четное.
                //Считаем четные и нечетные числа chet- счетчик четных чисел, nechet - счетчик нечетных чисел.
                int nechet = 0, chet = 0;
                for (int j = 0; j < n; j++) {
                    if ((height[j] % 2) == 0) {
                        chet++;
                        f= j+1;
                    } else {
                        nechet++;
                        d=j+1;
                    }
                }

                //определяем можем ли поменять пары игроков для валидного итога
                if (chet == nechet) {
                    int v = 1;
                    for (int j = 1; j < n; j++) {

                        if ((height[j]) > (height[j - 1])) {
                            v++;
                        }
                    }
                    if (v == n) {
                        System.out.print("-1 -1");
                    } else {
                        System.out.print(d + " " + f);
                    }
                } else {
                    System.out.print("-1 -1");
                }
            }
    }
}
