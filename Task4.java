import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        // Принимаем n  и k
       boolean QA = true;
        int n = x.nextInt(), k = x.nextInt();
        //числа и записываем в двумерную матрицу.
        int[][] a = new int[n][3];
        if (n >= 1 && n <= 1000 && k >= 1 && k <= 10000) {
            for (int i = 0; i < n; i++) {
                a[i][0] = x.nextInt();
                if (a[i][0] <= 1000000000 && a[i][0] > 0) {
                    if (a[i][0] > 9) {
                        boolean vvod = false;
                        int m = 10;
                        while (!vvod) {
                            vvod = true;
                            a[i][2] = a[i][0] % m;
                            if (a[i][0] != a[i][2]) {
                                a[i][1] = (m * 9) - (a[i][0] - (a[i][0] % m));
                                vvod = false;
                            }
                            m = m * 10;
                        }
                    } else {
                           a[i][1] = 9 - a[i][0];
                    }
                }
            }
        } else {
           QA = false;
        }

        // Сортировка
        if (QA) {
            boolean sort = false;
            while (!sort) {
                sort = true;
                for (int i = 0; i < n; i++) {
                    if (a[i][1] < a[i + 1][1]) {
                        int g = a[i][0];
                        int w = a[i][1];
                        a[i][0] = a[i + 1][0];
                        a[i][1] = a[i + 1][1];
                        a[i + 1][0] = g;
                        a[i + 1][1] = w;
                        sort = false;
                    }
                }
            }

            long summ = 0;
            for (int i = 0; i < k; i++) {
                summ = summ + a[i][1];
            }
            System.out.print(summ);
        }
    }
}
